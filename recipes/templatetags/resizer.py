from django import template

register = template.Library()


# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/05-model-topics/72-dynamic-resizing.md
def resize_to(ingredient, target):
    # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings
    # properties
    original_serving = ingredient.recipe.servings

    # If the servings from the recipe is not None
    # and the value of target is not None
    if original_serving is not None and target is not None:
        try:
            # calculate the ratio of target over servings
            multiplier = int(target) / original_serving
            # return the ratio multiplied by the ingredient's amount
            resized_amount = ingredient.amount * multiplier
            return resized_amount
        except:
            pass
    # return the original ingredient's amount since nothing else worked
    return ingredient.amount


register.filter(resize_to)
