from typing import List
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from recipes.forms import RatingForm

# we originally needed this import when we had function-based views.
# but once we moved to class-based views (RecipteCreateView and RecipeUpdateView),
# these are no longer needed
# from recipes.forms import RecipeForm
from recipes.models import Recipe

from recipes.models import Ingredient, ShoppingItem
from django.db import IntegrityError

from django.views.decorators.http import require_http_methods


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 4


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        # Create a new empty list and assign it to a variable
        shoppinglist = []
        # For each item in the user's shopping items, which we
        # can access through the code
        # self.request.user.shopping_items.all()
        # (self is passed in as a parameter to this get_context_data method)
        # (request.user is the current user)
        # (shoppingitems is the related_name we gave to the user property in the ForeignKey within ShoppingItem model)
        # (.all just to get all of shoppingitem instances)
        for item in self.request.user.shoppingitems.all():
            # Add the shopping item's food to the list
            shoppinglist.append(item.food_item)
        # Put that list into the context
        context["shopping_list"] = shoppinglist

        # Strategy: Get the resize request, if it exists, from the request.GET dictionary and put it into the context
        # The self.request.GET property is a dictionary
        # Get the value out of there associated with the key "servings_resize_request_by_user_on_page"
        # (this key is the "name= " in the input tag that I myself assigned it on recipes/detail.html -- see around line 25)
        servings_resize_requested_number = self.request.GET.get(
            "servings_resize_request_by_user_on_page"
        )
        # Store in the context dictionary with the key "servings"
        context["requested_servings"] = servings_resize_requested_number

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    # success_url = reverse_lazy("recipes_list")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)

    def form_valid(self, form):
        recipe = form.save(commit=False)
        recipe.author = self.request.user
        recipe.save()
        return redirect("recipe_detail", pk=recipe.id)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


# This view handles only HTTP POST requests, which means there's no HTML template for it.
# It should create a ShoppingItem instance in the database based on the current user and the value of the submitted "ingredients_id" value.
# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/65-shopping-lists.md
# To make it so a function view only handles POST requests, you can use the http_require_methods decorator.
# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/67-shopping-lists.md
# (this affects the behavior of the page when the user types /recipes/shopping_items/create on the url address bar)
# (with this @require_http_methods, the user will get a 405 error "The page isn't working")
# (without this @require_http_methods, idk what it would show, but the developer sees a yellow page of sadness: )
# ("DoesNotExist at /recipes/shopping_items/create/"           "Ingredient matching query does not exist."")
@require_http_methods(["POST"])
def create_shopping_item(request):
    # Get the value for the "ingredient_id" from the
    # request.POST dictionary using the "get" method
    ingredientid = request.POST.get("ingredient_id")

    # Get the specific ingredient from the Ingredient model
    # using the code
    # Ingredient.objects.get(id=the value from the dictionary)
    ingredient = Ingredient.objects.get(id=ingredientid)

    # Get the current user which is stored in request.user
    currentuser = request.user

    try:
        # Create the new shopping item in the database
        # using ShoppingItem.objects.create(
        #   food_item= the food item on the ingredient,
        #   user= the current user
        # )
        ShoppingItem.objects.create(
            food_item=ingredient.food,
            user=currentuser,
        )
    except IntegrityError:
        pass

    # Go back to the recipe page with a redirect
    # to the name of the registered recipe detail
    # path with code like this
    # return redirect(
    #     name of the registered recipe detail path,
    #     pk=id of the ingredient's recipe
    # )
    return redirect("recipe_detail", pk=ingredient.recipe.id)


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


# This view handles only HTTP POST requests, which means there's no HTML template for it.
# It should create a ShoppingItem instance in the database based on the current user and the value of the submitted "ingredients_id" value.
# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/65-shopping-lists.md
# To make it so a function view only handles POST requests, you can use the http_require_methods decorator.
# https://learn-2.galvanize.com/cohorts/3189/blocks/1849/content_files/build/04-view-topics/67-shopping-lists.md
# (this affects the behavior of the page when the user types /recipes/shopping_items/create on the url address bar)
# (with this @require_http_methods, the user will get a 405 error "The page isn't working")
@require_http_methods(["POST"])
def delete_all_shopping_items(request):
    # Delete all of the shopping items for the user
    # using code like
    # ShoppingItem.objects.filter(user=the current user).delete()
    ShoppingItem.objects.filter(user=request.user).delete()

    # Go back to the shopping item list with a redirect
    # to the name of the registered shopping item list
    # path with code like this
    # return redirect(
    #     name of the registered shopping item list path
    # )
    return redirect("shoppingitems_list")
