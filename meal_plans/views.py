from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

from meal_plans.models import MealPlan

from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        # Save the meal plan, but don't put it in the database
        plan = form.save(commit=False)
        # Assign the owner to the meal plan
        plan.owner = self.request.user
        # Now, save it to the database
        plan.save()
        # Save all of the many-to-many relationships
        form.save_m2m()
        # Redirect to the detail page for the meal plan
        return redirect("mealplan_detail", pk=plan.id)

    # The below 4 lines would have exactly the same effect as
    # the above form_valid method, except for where the user is redirected to

    # success_url = reverse_lazy("mealplans_list")

    # def form_valid(self, form):
    #     form.instance.owner = self.request.user
    #     return super().form_valid(form)


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]
    # success_url = reverse_lazy("mealplans_list")

    def get_success_url(self) -> str:
        return reverse_lazy("mealplan_detail", args=[self.object.id])

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("mealplans_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)
